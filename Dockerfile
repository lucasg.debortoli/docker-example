FROM python:3.11

ADD example.py .

CMD ["python", "./example.py"]
